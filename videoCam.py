import cv2
import picamera
import picamera.array
face_cascade = cv2.CascadeClassifier('cascade.xml')
#face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
with picamera.PiCamera() as camera:
    with picamera.array.PiRGBArray(camera) as frame:
        camera.resolution = (320, 240)
        camera.vflip = True
        while True:
            camera.capture(frame, 'bgr', use_video_port=True)
            image = frame.array
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            for (x,y,w,h) in faces:
                cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = image[y:y+h, x:x+w]
            cv2.imshow('frame', image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            frame.seek(0)
            frame.truncate()
        cv2.destroyAllWindows()
